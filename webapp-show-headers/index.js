// index.js

const express = require("express");

const app = express();
const port = process.env.PORT || "8001";

app.get("/", (req, res) => {
  console.log(req.headers);
  res.json(req.headers);
});

app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`);
});