# auth0-nginx-auth-proxy

Create a regular web application in your Auth0 tenancy

Configure the Auth0 application with the following allowed callback url:
http://localhost:8010/callback

Configure the Auth0 application with the following allowed logout url:
http://localhost:8010/logout

Create auth0-proxy/.env

Add the following to .env

	AUTH0_CLIENT_ID=<your Auth0 application client id>
	AUTH0_DOMAIN=<your Auth0 domain>
	AUTH0_CLIENT_SECRET=<your Auth0 application secret>
	AUTH0_CALLBACK_URL=http://localhost:8010/callback


Run

	docker-compose up


