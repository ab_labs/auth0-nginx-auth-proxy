// index.js
require("dotenv").config();

const path = require("path");
const util = require("util");
const express = require("express");
const expressSession = require("express-session");
const redisStore = require('connect-redis')(expressSession);
const passport = require("passport");
const Auth0Strategy = require("passport-auth0");
const authRouter = require("./auth");

const app = express();
const port = process.env.PORT || "8000";


const strategy = new Auth0Strategy(
  {
    domain: process.env.AUTH0_DOMAIN,
    clientID: process.env.AUTH0_CLIENT_ID,
    clientSecret: process.env.AUTH0_CLIENT_SECRET,
    callbackURL:
      process.env.AUTH0_CALLBACK_URL || "http://localhost:8000/callback"
  },
  function(accessToken, refreshToken, extraParams, profile, done) {
    /**
     * Access tokens are used to authorize users to an API 
     * (resource server)
     * accessToken is the token to call the Auth0 API 
     * or a secured third-party API
     * extraParams.id_token has the JSON Web Token
     * profile has all the information from the user
     */

     console.log("\n\n" + extraParams.id_token + "\n\n");

    return done(null, profile);
    // return done(null, extraParams.id_token);
  }
);

const options = {
  host: 'redis',
  port: 6379,
  logErrors: true
};

const session = {
  secret: "omeomy",
  name: '_mebf',
  cookie: {
    maxAge: 60000
  },
  resave: false,
  saveUninitialized: false,
  store: new redisStore(options)
};

if (app.get("env") === "production") {
  session.cookie.secure = true; // Serve secure cookies, requires HTTPS
}

app.use(expressSession(session));
passport.use(strategy);
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});

app.use((req, res, next) => {
  res.locals.isAuthenticated = req.isAuthenticated();
  // experimenting with viewing session object
  console.log("session: " + util.inspect(req.session));
  next();
});

const secured = (req, res, next) => {
  if (req.user) {
    return next();
  } 
  res.sendStatus(401);
};

app.use("/", authRouter);

app.get("/validate", secured, (req, res, next) => {
  console.log("cookie expires in: " + req.session.cookie.maxAge);

  if (req.isAuthenticated) {
    res.setHeader('upstream_user_id', req.session.passport.user.id);
    res.set('user', req.session.passport.user.id);
  }
  res.sendStatus(200);
});

app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`);
});
